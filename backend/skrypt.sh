#!/bin/env bash

sudo cp skrypt.sh /usr/local/bin
python3 -m venv ~/env
set -e
source /home/vagrant/env/bin/activate
cd /vagrant
pip3 install -r requirements.txt
cd elearn_app_project
python3 manage.py runserver 0.0.0.0:8000
