from django.urls import path, include
from auth_system import views
from .views import CustomTokenObtainPairView   

urlpatterns = [
	path('', include('djoser.urls')),
    path('', include('djoser.urls.authtoken')),
    
]