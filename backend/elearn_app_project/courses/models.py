import string
from random import random
from random import choices
from string import digits
from django.db import models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django_userforeignkey.models.fields import UserForeignKey
from random import uniform, random, choice, sample
from django.contrib.contenttypes.fields import ContentType


def create_code():
    letters_and_digits = string.ascii_letters + string.digits
    return ''.join((choice(letters_and_digits) for i in range(6)))
        

class Course(models.Model):
    author = UserForeignKey(auto_user_add=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    body = models.TextField(max_length=5000, null=False, blank=False)
    date_published = models.DateTimeField(auto_now_add=True, verbose_name="date published")
    date_updated = models.DateTimeField(auto_now=True, verbose_name="date updated")
    student = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='courses_joined', blank=True)
    code = models.CharField(max_length=6, default=create_code, unique=True)

    def __str__(self):
        return self.title

class Dashboard(models.Model):
    course = models.ForeignKey(Course, related_name='dashboards', on_delete=models.CASCADE)
    title = models.CharField(max_length=50, null=False, blank=False)
    body  = models.TextField(max_length=5000, null=False, blank=False)
    date_published = models.DateTimeField(auto_now_add=True, verbose_name="date published")
    date_updated = models.DateTimeField(auto_now=True, verbose_name="date updated")
    author = UserForeignKey(auto_user_add=True, on_delete=models.CASCADE)
    file = models.FileField(upload_to='resource/%Y/', null=True, blank=True)

    def __str__(self):
        return self.title


