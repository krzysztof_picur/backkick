from django.urls import path, include
from rest_framework import routers
from . import views
from courses.api.views import CourseViewSet, CurrentUserCoursesView, TeacherCoursesView, CoursePostDetail, DashboardPostList
from django.urls import path

app_name = 'courses'

router = routers.DefaultRouter()
router.register(r'teacher_courses', TeacherCoursesView, 'teacher_courses')
router.register(r'list', CourseViewSet, 'list_courses')
router.register(r'my_courses', CurrentUserCoursesView, 'my_courses')
router.register(r'alldashboards', DashboardPostList, 'alldashboards')


urlpatterns = [
	
	path('courses/<pk>/enroll/', views.CourseEnrollView.as_view(), name='course_enroll'),
	path('', include(router.urls)),
	#path('alldashboards/', DashboardPostList.as_view()),

]