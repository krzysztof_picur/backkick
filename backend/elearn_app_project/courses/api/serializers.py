from rest_framework import serializers
from courses.models import Course, Dashboard
from auth_system.serializers import UserCreateSerializer
from courses.models import Dashboard



class DashboardPostSerializer(serializers.ModelSerializer):
	author = serializers.CharField(source='author.get_full_name', read_only=True)
	class Meta:
		model = Dashboard
		fields = ['id','author','title', 'body', 'file', 'date_updated', 'course','date_published']


class CourseSerializer(serializers.ModelSerializer):
	author = serializers.CharField(source='author.get_full_name', read_only=True)
	dashboards = DashboardPostSerializer(many=True, read_only=True)
	student = serializers.StringRelatedField(many=True, read_only=True)

	class Meta:
		model = Course
		fields = ['id', 'author', 'title', 'body', 'code', 'student', 'date_updated', 'dashboards']


