from django.shortcuts import get_object_or_404
from auth_system.views import CustomTokenObtainPairView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.conf import settings
from auth_system.models import User
from rest_framework import viewsets
from .serializers import CourseSerializer,DashboardPostSerializer
from rest_framework import permissions
from courses.models import Course, Dashboard
from rest_framework.decorators import api_view, permission_classes
from auth_system.models import User
from rest_framework import generics, permissions
from courses.permissions import IsAuthorOrReadOnly


class CourseEnrollView(APIView):
	permission_classes = (IsAuthenticated,)

	def post(self, request, pk, format=None):
		course = get_object_or_404(Course, pk=pk)
		request.POST.get('code')
		course.student.add(request.user)
		return Response({'enrolled': True})
			

	
class CourseViewSet(viewsets.ModelViewSet):
	permission_classes = (permissions.IsAuthenticated,)
	queryset = Course.objects.all()	
	serializer_class = CourseSerializer

	
class CurrentUserCoursesView(viewsets.ModelViewSet):
	
	permission_classes = (permissions.IsAuthenticated,)
	queryset = Course.objects.all()
	serializer_class = CourseSerializer

	def get_queryset(self):
		
		user = self.request.user
		return Course.objects.filter(student=user)


class CoursePostDetail(viewsets.ModelViewSet):
	permission_classes = (IsAuthorOrReadOnly,)
	queryset = Course.objects.all()
	serializer_class = CourseSerializer


class TeacherCoursesView(viewsets.ModelViewSet):
	permission_classes = (permissions.IsAuthenticated,)
	queryset = Course.objects.all()
	serializer_class = CourseSerializer

	
	def get_queryset(self):
		user = self.request.user
		return Course.objects.filter(author=user)


class DashboardPostList(viewsets.ModelViewSet):
	permission_classes = (permissions.IsAuthenticated,)
	queryset = Dashboard.objects.all()
	serializer_class = DashboardPostSerializer
	
class DashboardPostDetail(viewsets.ModelViewSet):
	permission_classes = (IsAuthorOrReadOnly,)
	queryset = Dashboard.objects.all()
	serializer_class = DashboardPostSerializer