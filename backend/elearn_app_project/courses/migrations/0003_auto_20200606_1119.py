# Generated by Django 3.0.6 on 2020-06-06 11:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0002_course_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='code',
            field=models.CharField(default='5e43b5c2', max_length=6, unique=True),
        ),
    ]
