from django.contrib import admin
from .models import Course, Dashboard
# Register your models here.


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
	list_display = ['title']
	search_fields = ['title', 'body']

@admin.register(Dashboard)
class DashboardAdmin(admin.ModelAdmin):
	list_display = ['title']
	search_fields = ['title', 'body']